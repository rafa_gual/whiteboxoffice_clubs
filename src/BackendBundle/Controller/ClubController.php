<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Club;
use BackendBundle\Form\ClubType;
use BackendBundle\Repository\ClubRepository;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ClubController
 * @package BackendBundle\Controller
 *
 * @RouteResource("club")
 */
class ClubController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get a collection of Clubs
     *
     * @QueryParam(name="name", nullable=true, description="Part of Club Name")
     * @QueryParam(name="nickname", nullable=true, description="Part of Club Nickname")
     *
     * @ApiDoc(
     *     description="Returns a collection of Clubs",
     *     section="clubs",
     *     statusCodes={
     *        200="Returned when successful",
     *        404="Not found"
     *      }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function cgetAction(ParamFetcher $paramFetcher): View
    {
        $clubName = null;
        $clubNickname = null;

        foreach ($paramFetcher->all() as $paramKey => $paramValue) {
            switch ($paramKey) {
                case "name":
                    $clubName = $paramValue;
                    break;
                case "nickname":
                    $clubNickname = $paramValue;
                    break;
            }
        }

        /**
         * @var Club[]
         */
        $clubs = $this->getClubRepository()->findFilteredClubs($clubName, $clubNickname);

        if ($clubs == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        return new View($clubs, Response::HTTP_OK);
    }

    /**
     * Gets an individual Club
     *
     * @ApiDoc(
     *     description="Returns a Club",
     *     section="clubs",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="You must pass Club Id as parameter"}
     *      },
     *     statusCodes={
     *         200="Returned when successful",
     *         404="Not found"
     *     }
     * )
     *
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     */
    public function getAction(int $id): View
    {
        /**
         * @var Club
         */
        $club = $this->getClubRepository()->findClubById($id);

        if ($club == null) {
            throw new HttpException(404, "Not existant club");
            return new View(null, Response::HTTP_NOT_FOUND);
        }
        return new View($club, Response::HTTP_OK);
    }

    /**
     * Post a new club
     *
     * @ApiDoc(
     *     decription="Create a new Club",
     *     section="clubs",
     *     input="BackendBundle\Form\ClubType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="You must pass Club Id as parameter"}
     *      },
     *     statusCodes={
     *         201 = "Returned when a new BlogPost has been successful created",
     *         400 = "Return when data errors",
     *         404 = "Return when not found",
     *         500 = "Return when internal errors"
     *     }
     * )
     *
     * @param Request $request
     * @return View|\Symfony\Component\Form\FormInterface
     */
    public function postAction(Request $request): object
    {
        $form = $this->createForm(ClubType::class, null, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @var Club
         */
        $club = $form->getData();

        $this->getClubRepository()->saveClub($club);

        $routeOptions = [
            'id' => $club->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_club', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * Edit a current club
     *
     * @ApiDoc(
     *     decription="Edit a current Club",
     *     section="clubs",
     *     input="BackendBundle\Form\ClubType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="You must pass Club Id as parameter"}
     *      },
     *     statusCodes={
     *         204 = "Returned when an existing Club has been successful updated",
     *         400 = "Return when data errors",
     *         404 = "Return when not found",
     *         500 = "Return when internal errors"
     *     }
     * )
     *
     * @param Request $request
     * @param int $id
     * @return View|\Symfony\Component\Form\FormInterface
     */
    public function putAction(Request $request, int $id): object
    {

        /**
         * @var Club
         */
        $club = $this->getClubRepository()->findClubById($id);

        if ($club == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ClubType::class, $club, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $this->getClubRepository()->saveClub($club);

        $routeOptions = [
            'id' => $club->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_club', $routeOptions, Response::HTTP_NO_CONTENT);
    }

    /**
     * Edit part of a current club
     *
     * @ApiDoc(
     *     decription="Edit some fields of a current Club",
     *     section="clubs",
     *     input="BackendBundle\Form\ClubType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="You must pass Club Id as parameter"}
     *      },
     *     statusCodes={
     *         204 = "Returned when an existing Club has been successful updated",
     *         400 = "Return when data errors",
     *         404 = "Return when not found",
     *         500 = "Return when internal errors"
     *     }
     * )
     *
     * @param Request $request
     * @param int $id
     * @return View|\Symfony\Component\Form\FormInterface
     */
    public function patchAction(Request $request, int $id): object
    {

        /**
         * @var Club
         */
        $club = $this->getClubRepository()->findClubById($id);

        if ($club == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ClubType::class, $club, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $this->getClubRepository()->saveClub($club);

        $routeOptions = [
            'id' => $club->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_club', $routeOptions, Response::HTTP_NO_CONTENT);
    }

    /**
     * Delete a club
     *
     * @ApiDoc(
     *     decription="Delete an existant Club",
     *     section="clubs",requirements={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="You must pass Club Id as parameter"}
     *      },
     *     statusCodes={
     *         204 = "Returned when an existing Club has been successful updated",
     *         404 = "Return when not found"
     *     }
     * )
     *
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction(int $id): View
    {

        /**
         * @var Club
         */
        $club = $this->getClubRepository()->findClubById($id);

        if ($club == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $this->getClubRepository()->deleteClub($club);

        return new View(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @return ClubRepository
     */
    private function getClubRepository(): ClubRepository
    {
        /**
         * @var ClubRepository
         */
        $ClubRepository = ($this->get('entity_repository.club'));
        return $ClubRepository;
    }
}
