<?php

namespace BackendBundle\Repository;

use BackendBundle\Entity\Club;
use Doctrine\ORM\EntityRepository;

class ClubRepository extends EntityRepository
{
    /**
     * Return several Clubs filtered by name or nickname
     * Asume that is a like filter and returns both matches name plus nickname
     *
     * @param string|null $clubName
     * @param string|null $clubNickname
     * @return array
     */
    public function findFilteredClubs(string $clubName = null, string $clubNickname = null): array
    {
        $dql = "SELECT c FROM BackendBundle:Club c";
        if ($clubName) {
            $dql .= " WHERE c.name LIKE :name";
        }
        if ($clubNickname) {
            $dql .= ($clubName) ? " OR " : " WHERE ";
            $dql .= 'c.nickname LIKE :nickname';
        }

        $query = $this->_em->createQuery($dql);
        if ($clubName) {
            $query->setParameter('name', '%' . $clubName . '%');
        }
        if ($clubNickname) {
            $query->setParameter('nickname', '%' . $clubNickname . '%');
        }

        return $query->getResult();
    }

    /**
     * Find a Club by the id
     *
     * @param int $id
     * @return Club|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findClubById(int $id): ?Club
    {
        $dql = "SELECT c FROM BackendBundle:Club c
                WHERE c.id = :id";

        $query = $this->_em->createQuery($dql);
        $query->setParameter('id', $id);

        return $query->getOneOrNullResult();
    }

    /**
     * Save a Club
     *
     * @param Club $club
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveClub(Club $club)
    {
        $this->_em->persist($club);
        $this->_em->flush();
    }

    /**
     * Delete a Club
     *
     * @param Club $club
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteClub(Club $club)
    {
        $this->_em->remove($club);
        $this->_em->flush();
    }
}
