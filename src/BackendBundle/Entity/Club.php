<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Club
 *
 * @ORM\Table(name="clubs")
 * @ORM\Entity(repositoryClass="BackendBundle\Repository\ClubRepository")
 */
class Club
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=150)
     */
    private $nickname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_foundation", type="datetime")
     */
    private $dateFoundation;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return \DateTime
     */
    public function getDateFoundation()
    {
        return $this->dateFoundation;
    }

    /**
     * @param \DateTime $dateFoundation
     */
    public function setDateFoundation($dateFoundation)
    {
        $this->dateFoundation = $dateFoundation;
    }
}
