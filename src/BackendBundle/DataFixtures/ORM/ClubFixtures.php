<?php

namespace BackendBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use BackendBundle\Entity\Club;

class ClubFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($index = 0; $index < 20; $index++) {
            $club = new Club();
            $club->setId($index+1);
            $club->setName('club '. ($index + 1));
            $club->setNickname('nickname '. ($index + 1));
            $club->setDateFoundation(new \DateTime('now + '. $index .' day'));
            $manager->persist($club);
        }

        $manager->flush();
    }
}
