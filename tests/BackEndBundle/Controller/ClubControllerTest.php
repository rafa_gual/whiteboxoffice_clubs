<?php

namespace BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\Client;
use Symfony\Component\Console\Input\StringInput;
use GuzzleHttp\Exception\RequestException;

class ClubControllerTest extends WebTestCase
{
    protected static $application;
    protected $clientGuzzle;

    public function setUp():void
    {
        self::runCommand('doctrine:fixtures:load --purge-with-truncate');

        $this->clientGuzzle = new Client(
            ['base_uri' => 'http://localhost/whiteboxOfficeClubs/web/app_dev.php/']
        );
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    public function testGetClub()
    {
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs/2');

            $club = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }


        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 2', $club->name);
        $this->assertObjectHasAttribute('nickname', $club);
        $this->assertEquals('nickname 2', $club->nickname);
        $this->assertObjectHasAttribute('date_foundation', $club);
    }

    public function testGetAllClubs()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
            $club = $clubs[3];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(20, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 4', $club->name);
        $this->assertObjectHasAttribute('nickname', $club);
        $this->assertEquals('nickname 4', $club->nickname);
        $this->assertObjectHasAttribute('date_foundation', $club);
    }

    public function testGetClubsByName()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?name=2');
            $clubs = json_decode($response->getBody());
            $club = $clubs[1];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(3, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 12', $club->name);
        $this->assertObjectHasAttribute('nickname', $club);
        $this->assertEquals('nickname 12', $club->nickname);
        $this->assertObjectHasAttribute('date_foundation', $club);
    }

    public function testGetClubsByNickName()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?nickname=3');
            $clubs = json_decode($response->getBody());
            $club = $clubs[1];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(2, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 13', $club->name);
        $this->assertObjectHasAttribute('nickname', $club);
        $this->assertEquals('nickname 13', $club->nickname);
        $this->assertObjectHasAttribute('date_foundation', $club);
    }

    public function testGetClubsByNameOrNickName()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?nickname=3&name=2');
            $clubs = json_decode($response->getBody());
            $club = $clubs[1];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(5, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club 3', $club->name);
        $this->assertObjectHasAttribute('nickname', $club);
        $this->assertEquals('nickname 3', $club->nickname);
        $this->assertObjectHasAttribute('date_foundation', $club);
    }

    public function testGetClubsWithNoExistantName()
    {
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs?name=test');
            $clubs = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
    }

    public function testGetInexistantClub()
    {
        $club = null;

        try {
            $response = $this->clientGuzzle->get('club/56');
            $club = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(null, $club);
    }

    public function testPostClub()
    {

        //POST
        $dataJson = [
            'name' => 'club post',
            'nickname' => 'club añadido con post',
            'date_foundation' => '2019-06-03'
        ];

        try {
            $response = $this->clientGuzzle->post(
                'clubs',
                [ 'json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('Created', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //Tras el POST
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
            $club = $clubs[20];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(21, count($clubs));
        $this->assertObjectHasAttribute('name', $club);
        $this->assertEquals('club post', $club->name);
        $this->assertObjectHasAttribute('nickname', $club);
        $this->assertEquals('club añadido con post', $club->nickname);
        $this->assertObjectHasAttribute('date_foundation', $club);
        $this->assertEquals('2019-06-03T00:00:00+02:00', $club->date_foundation);
    }

    public function testPostWithNoNameClub()
    {
        //POST
        $dataJson = [
            'nickname' => 'club añadido con post',
            'date_foundation' => '2019-06-05'
        ];

        try {
            $response = $this->clientGuzzle->post(
                'clubs',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Internal Server Error', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //Tras el POST
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(20, count($clubs));
    }

    public function testPostNoValidDateClub()
    {
        //POST
        $dataJson = [
            'name' => 'club post',
            'nickname' => 'club añadido con post',
            'date_foundation' => '02/06/2019'
        ];

        try {
            $response = $this->clientGuzzle->post(
                'clubs',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('Bad Request', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //Tras el POST
        $clubs = null;
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(20, count($clubs));
    }

    public function testPutClub()
    {

        //PUT
        $dataJson = [
            'name' => 'club put',
            'nickname' => 'club modificado con put',
            'date_foundation' => '2019-06-01'
        ];

        try {
            $response = $this->clientGuzzle->put(
                'clubs/1',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('No Content', $response->getReasonPhrase());

        //Tras el PUT
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs/1');
            $club = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals('club put', $club->name);
        $this->assertEquals('club modificado con put', $club->nickname);
        $this->assertEquals('2019-06-01T00:00:00+02:00', $club->date_foundation);
    }

    public function testPutInexistantClub()
    {
        //PUT
        $dataJson = [
            'name' => 'club put',
            'nickname' => 'club modificado con put',
            'date_foundation' => '2019-06-01'
        ];

        try {
            $response = $this->clientGuzzle->put(
                'clubs/25',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
    }

    public function testPutWithNoNameClub()
    {

        //PUT
        $dataJson = [
            'nickname' => 'club modificado con put',
            'date_foundation' => '2019-06-01'
        ];

        try {
            $response = $this->clientGuzzle->put(
                'clubs/1',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Internal Server Error', $response->getReasonPhrase());
    }

    public function testPutNoValidDateClub()
    {
        //PUT
        $dataJson = [
            'name' => 'club put',
            'nickname' => 'club añadido con post',
            'date_foundation' => '02/06/2019'
        ];

        try {
            $response = $this->clientGuzzle->put(
                'clubs/1',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('Bad Request', $response->getReasonPhrase());
    }

    public function testPatchClub()
    {

        //PATCH
        $dataJson = [
            'name' => 'club put',
            'nickname' => 'club modificado con put',
            'date_foundation' => '2019-06-01'
        ];

        try {
            $response = $this->clientGuzzle->patch(
                'clubs/1',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('No Content', $response->getReasonPhrase());

        //Tras el PUT
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs/1');
            $club = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals('club put', $club->name);
        $this->assertEquals('club modificado con put', $club->nickname);
        $this->assertEquals('2019-06-01T00:00:00+02:00', $club->date_foundation);
    }

    public function testPatchInexistantClub()
    {
        //PATCH
        $dataJson = [
            'name' => 'club put',
            'date_foundation' => '2019-06-01'
        ];

        try {
            $response = $this->clientGuzzle->patch(
                'clubs/25',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
    }

    public function testPatchWithNoNameClub()
    {

        //PATCH
        $dataJson = [
            'nickname' => 'club modificado con put',
            'date_foundation' => '2019-06-01'
        ];

        try {
            $response = $this->clientGuzzle->patch(
                'clubs/1',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('No Content', $response->getReasonPhrase());

        //Tras el PUT
        $club = null;

        try {
            $response = $this->clientGuzzle->get('clubs/1');
            $club = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals('club 1', $club->name);
        $this->assertEquals('club modificado con put', $club->nickname);
        $this->assertEquals('2019-06-01T00:00:00+02:00', $club->date_foundation);
    }

    public function testPatchNoValidDateClub()
    {
        //patch
        $dataJson = [
            'name' => 'club put',
            'nickname' => 'club añadido con post',
            'date_foundation' => '02/06/2019'
        ];

        try {
            $response = $this->clientGuzzle->patch(
                'clubs/1',
                ['json' =>
                    $dataJson
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('Bad Request', $response->getReasonPhrase());
    }

    public function testDeleteClub()
    {
        $clubs = null;
        $club = null;

        //CLUBS ANTES DE BORRAR
        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
            $club = $clubs[10];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(20, count($clubs));
        $this->assertEquals('club 11', $club->name);

        //BORRO UN CLUB
        try {
            $this->clientGuzzle->delete('clubs/4');

            //CLUBS DESPUES DE BORRAR
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
            $club = $clubs[10];
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);
        $this->assertEquals(19, count($clubs));
        $this->assertEquals('club 12', $club->name);
    }

    public function testDeleteInexistantClub()
    {

        //BORRO UN CLUB
        try {
            $this->clientGuzzle->delete('clubs/22');
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
        $this->assertEquals('application/json', $response->getHeaders()["Content-Type"][0]);

        //CLUBS DESPUES DE BORRAR
        try {
            $response = $this->clientGuzzle->get('clubs');
            $clubs = json_decode($response->getBody());
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->assertEquals(20, count($clubs));
    }
}
